! checks if the number is a factor of 360.
      subroutine checksn (na1)
      integer na1,na2,n3
      logical a_rem
      na3=na1
      a_rem=.true.
      na1=abs(na1)
      if (na1.eq.0) then
       na1=30; a_rem=.false.
      elseif (na1.gt.360) then
       na1=360; a_rem=.false.
      endif

      do while (a_rem)
       na2=mod(360,na1)
       if (na2.GT.0) then
         na1=na1+1
       else
        a_rem=.false.
       endif
      enddo
     
      if (na3.ne.na1) write(*,*)"Rounded up to nearest factor of 360: ",
     & na1 
      return
      end       
 

      subroutine n_from_l(l_in,n_bs,n_out,n_set)

      integer l_in,n_out(3),n_bs(4,4),n1,n2,n_set
 
c      write(*,*) "Backtransforming ",l_in," in grid ",
c     &     (n_bs(n2),n2=1,3)
      l_in=l_in-1
      n_out(1)=1+l_in/(n_bs(2,n_set)*n_bs(3,n_set))
      n1=mod(l_in,(n_bs(2,n_set)*n_bs(3,n_set)))
      n_out(2)=1+n1/n_bs(3,n_set)
      n_out(3)=mod(n1,n_bs(3,n_set))+1

      return
      end

      integer function l_from_n(n_in,n_bs,n_set)
      integer n_in(3),n_bs(4,4),n_set    

      l_from_n=n_in(3)+(n_in(2)-1)*n_bs(3,n_set)+
     &  (n_in(1)-1)*n_bs(2,n_set)*n_bs(3,n_set)
      return
      end

! diffrnt is a function for differentiating phase. Non-phase data should not go in here.
      double precision function diffrnt (d1,d2,d3,dgap)
      double precision d1,d2,d3,ddiff,dgap
      logical a_dble
c      logical a_inv(3) ! mark if the numbers are valid for differentiating

      a_dble=.true.
c      if (d1.eq.1000.) a_inv(1)=.true.
c      if (d2.eq.1000.) a_inv(2)=.true.
c      if (d3.eq.1000.) a_inv(3)=.true.

      ddiff=(d3-d1)
      if (d1.eq.1000.) then
       ddiff=(d3-d2)
       a_dble=.false.
      elseif (d3.eq.1000.) then
       ddiff=(d2-d1)
       a_dble=.false.
      else
       ddiff=(d3-d1)
      endif

      if ((d1.eq.1000.).and.(d3.eq.1000.)) then
        diffrnt=0.
        return
      elseif (  ((d1.eq.1000.).or.(d3.eq.1000.)) .and.
     &           (d2.eq.1000.) ) then
        diffrnt=0.
        return
      endif

      if (ddiff.GT.3.1415926365) ddiff=ddiff-2*(3.1415926535)
      if (ddiff.LT.-3.1415926365) ddiff=ddiff+2*(3.1415926535)
      ddiff=ddiff/dgap
      if (a_dble) ddiff=ddiff/2.
      diffrnt=ddiff
      return;end
