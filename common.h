      COMMON XYZ,DR,DCUBE,ASYM,TITLE,NSYM,NAT,N,A_FILE
        character title(5)*30 ! titles/labels for the grid datasets.
        double precision xyz(40,3,5) ! atoms. (Number in nat(5).)
        character asym(40,5)*3 ! atomic symbols.
        integer nat(5) ! number of atoms
        integer nsym(40,5) ! Atomic numbers
        double precision dr(4,3,5) ! grid vector information
                                   ! first label is for which vector - 1,2,3 are x,y,z vectors
                                   ! 4 is origin
        integer n(4,5) ! grid size information
        double precision dcube(5,4500000) ! actual grid information
        logical A_FILE(0:5) ! does the file exist

