      subroutine reorient (na1,na2,na3,na4,a_XorY)
      include "common.h"
      integer na1,na2,na3,na4
      logical a_XorY ! true = X, false = Y
      integer ia,ib
      double precision d_NS(4)

      do ia=1,n(4,na1)
       if (a_XorY) then
        d_NS(1)=sqrt(0.5)*(dcube(na1,ia)+dcube(na3,ia))     ! Re[A]=Re[a]+Re[b]
        d_NS(3)=sqrt(0.5)*(dcube(na1,ia)-dcube(na3,ia))     ! Re[B]=Re[a]-Re[b]
        d_NS(2)=sqrt(0.5)*(dcube(na2,ia)+dcube(na4,ia))     ! Im[A]=Im[a]+Im[b]
        d_NS(4)=sqrt(0.5)*(dcube(na2,ia)-dcube(na4,ia))     ! Im[B]=Im[a]-Im[b]
       else
        d_NS(1)=sqrt(0.5)*(dcube(na3,ia)-dcube(na2,ia))     ! Re[A]=Re[b]-Im[a]
        d_NS(3)=sqrt(0.5)*(dcube(na3,ia)+dcube(na2,ia))     ! Re[A]=Re[b]+Im[a]
        d_NS(2)=sqrt(0.5)*(dcube(na4,ia)+dcube(na1,ia))     ! Im[A]=Im[b]+Re[a]
        d_NS(4)=sqrt(0.5)*(dcube(na4,ia)-dcube(na1,ia))     ! Im[A]=Im[b]-Re[a]
       endif
       do ib=1,4;dcube(ib,ia)=d_NS(ib);enddo
      enddo

      return;end

