! This subroutine adds/subtracts/multiplies/divides two cube files by each other.
! First 3 arguments are datasets, then title for new dataset, then an integer indicating which operation.
      subroutine mathops(na1,na2,na3,c_nt,n_op) !c_nt = new title
      include "common.h"
      integer na1,na2,na3
      integer n_op ! 1=add;2=subtract;3=multiply;4=divide
      character c_nt*40
      integer ia

      call duplicate(na1,na3,c_nt,.false.)

      select case(n_op)
       case(1)
        do ia=1,n(4,na1)
         dcube(na3,ia)=dcube(na1,ia)+dcube(na2,ia)
        enddo
       case(2)
        do ia=1,n(4,na1)
         dcube(na3,ia)=dcube(na1,ia)-dcube(na2,ia)
        enddo
       case(3)
        do ia=1,n(4,na1)
         dcube(na3,ia)=dcube(na1,ia)*dcube(na2,ia)
        enddo
       case(4)
        do ia=1,n(4,na1)
         if (dcube(na2,ia).eq.0) then
          dcube(na3,ia)=dcube(na1,ia)/0.00000001
         else
          dcube(na3,ia)=dcube(na1,ia)/dcube(na2,ia)
         endif
        enddo
      end select
      return
      end

! This subroutine rotates the complex function in the complex plane.
! It takes 3 arguments: the datasets corresponding to the real and complex functions,
! and the angle.
      subroutine rotation(na1,na2,d_theta)
      include "common.h"
      integer na1,na2
      double precision d_theta,thc,ths,dsc1,dsc2
      integer ia
      
      thc=cos(d_theta);ths=sin(d_theta)
      do ia=1,n(4,na1)
       dsc1=dcube(na1,ia);dsc2=dcube(na2,ia)
       dcube(na1,ia)=(thc*dsc1-ths*dsc2)
       dcube(na2,ia)=(ths*dsc1+thc*dsc2)
      enddo
      
      return
      end

! This subroutine calculates a kind of inertia tensor in the complex plane. It requires 
! two datasets, real and imaginary.
      subroutine inertial(na1,na2,d_theta)
      include "common.h"
      integer na1,na2,ia
      double precision d_theta
      double precision tens(4),pi_2,dev(2,4),dvec(2,2) 
      character answ*1
      logical a_answ

      a_answ=.true.
      pi_2=acos(0.)
      tens=0.
      do ia=1,n(4,na1)
       tens(1)=tens(1)+dcube(na1,ia)**2
       tens(2)=tens(2)+dcube(na2,ia)**2
       tens(3)=tens(3)+dcube(na1,ia)*dcube(na2,ia)
      enddo

      if (tens(3).eq.0.00) then
       d_theta=0.
      elseif ((tens(1)-tens(2)).eq.0.) then
       d_theta=0.5*pi_2
c       d_theta=0.25*3.1415926536
      else 
       d_theta=0.5*atan( (2*tens(3))/(tens(1)-tens(2) ) )
      endif 
      write(*,*) "Complex pseudo-inertial tensor: "
      write(*,*) tens(1),tens(3)
      write(*,*) tens(3),tens(2)
c      tens(4)=abs(tens(3)/(tens(2)+tens(1)))
 
      if (abs(tens(3)/(tens(2)+tens(1))).lt.0.0001) then
       write(*,*) "This matrix is diagonal (except numerical noise)."
       write (*,*) "Metric:",abs(tens(3)/(tens(2)+tens(1)))
       a_answ=.false.
       if (tens(1).LT.tens(2)) then
        write(*,*) "The datasets are the wrong way round. Take ",na2,
     & "as real and ",na1,"as imaginary."
       endif
      endif


!      write(*,*) d_theta, cos(d_theta),sin(d_theta)
      dvec(1,1)=cos(d_theta);dvec(1,2)=sin(d_theta)
      dvec(2,1)=cos(d_theta+pi_2);dvec(2,2)=sin(d_theta+pi_2)

      write(*,*) "Eigenvectors: "
      write(*,*) (dvec(1,ia),ia=1,2)
      write(*,*) (dvec(2,ia),ia=1,2)

      dev(1,1) = (tens(1)*dvec(1,1)+tens(3)*dvec(1,2))/dvec(1,1)
      dev(1,2) = (tens(3)*dvec(1,1)+tens(2)*dvec(1,2))/dvec(1,2)
      dev(2,1) = (tens(1)*dvec(2,1)+tens(3)*dvec(2,2))/dvec(2,1)
      dev(2,2) = (tens(3)*dvec(2,1)+tens(2)*dvec(2,2))/dvec(2,2)

      dev(1,3)=(dev(1,1)+dev(1,2))/2.
      dev(1,4)=abs(dev(1,1)-dev(1,3))
      dev(2,3)=(dev(2,1)+dev(2,2))/2.
      dev(2,4)=abs(dev(2,1)-dev(2,3))
      write(*,*) "Angle 1 :",d_theta
      write(*,*) "Angle 2 :",d_theta+pi_2
      write(*,*) "Vector 1: Eigenvalue ",dev(1,3),"+/-",dev(1,4)
      write(*,*) "Vector 2: Eigenvalue ",dev(2,3),"+/-",dev(2,4)

      if (dev(2,3).gt.dev(1,3)) d_theta=d_theta+pi_2

      do while (a_answ)
       write(*,*) "Would you like to rotate in the complex plane?"
       write(*,*) "[y/n]"
       read(*,*) answ
       if ( (answ.eq."Y").or.(answ.eq."y") ) then
        call rotation (na1,na2,-d_theta)
        a_answ=.false.
        write(*,*) "Rotated."
        tens=0.
        do ia=1,n(4,na1)
         tens(1)=tens(1)+dcube(na1,ia)**2
         tens(2)=tens(2)+dcube(na2,ia)**2
         tens(3)=tens(3)+dcube(na1,ia)*dcube(na2,ia)
        enddo
        write(*,*) "Complex pseudo-inertial tensor is now: "
        write(*,*) tens(1),tens(3)
        write(*,*) tens(3),tens(2)
       elseif ( (answ.eq."N").or.(answ.eq."n") ) then
        a_answ=.false.
       else
        write(*,*) "Answer not comprehended"
       endif
      enddo
      return
      end
   

! This subroutine scales a dataset by a scalar.
      subroutine scalecb(na1,d_sc)
      include "common.h"
      integer na1
      double precision d_sc
      integer ia

      do ia=1,n(4,na1); dcube(na1,ia)=dcube(na1,ia)*d_sc;enddo
      return
      end
    
! This subroutine raises a dataset to a power. (Usually squaring.)
      subroutine expcb(na1,na2)
      include "common.h"
      integer na1,na2
      integer ia

      do ia=1,n(4,na1); dcube(na1,ia)=dcube(na1,ia)**2;enddo
      return
      end
    
! Subroutine for phase
      subroutine phasemag (na1,na2,na3,na4) ! Real, imaginary, magnitude, phase
      include "common.h"

      integer ia
      integer nbin; double precision dbin,d_a

      integer n_freq(0:50)
      integer d_freq(0:50)
      call duplicate (na1,na3,"magnitude",.false.)
      call duplicate (na1,na4,"phase",.false.)

      open (unit=93,file="comp_dump", status="replace")
      open (unit=94,file="phmag_dump", status="replace")

      do ia=0,50; n_freq(ia)=0; d_freq(ia) = 0.0; enddo


      do ia=1,n(4,na1)
       dcube(3,ia)=sqrt (dcube(1,ia)**2+dcube(2,ia)**2) ! mag = sqrt [R^2+I^2}
       if (dcube(3,ia).eq.0.00000000000) then           ! if mag = 0 set phase to 1000.
        dcube(4,ia)=1000.                               ! this acts as flag when differentiating.
       else

        dcube(4,ia)=atan2(dcube(2,ia),dcube(1,ia))       ! phase=acos (R/mag)
c        dcube(4,ia)=acos(dcube(1,ia)/dcube(3,ia))       ! phase=acos (R/mag)
c        if (dcube(2,ia).lt.0.) dcube(4,ia)=-dcube(4,ia) ! but negative if I < 0
        dbin=(( dcube(4,ia) / 6.2831853 )+0.5) * 36
        nbin=nint(dbin)
        if (nbin .eq. 0) nbin=36
        n_freq(nbin)=n_freq(nbin)+1
        d_freq(nbin)=d_freq(nbin)+dcube(3,ia)
        write(93,*) dcube(1,ia),dcube(2,ia)
        write(94,*) dcube(4,ia),dcube(3,ia)
       endif

      enddo  
      close(93)
      close(94)
      open (unit=91,file="phase_distr",status="replace")
      open (unit=92,file="magphase_distr",status="replace")
      do ia=1,36
       d_a=real(ia-18) * 6.2831853
       write(91,*) d_a, dble( n_freq(ia))/dble (n(4,na1))
       write(92,*) d_a, dble( d_freq(ia))/dble (n(4,na1)) 
      enddo
      close(91);close(92)

      return
      end


! Subroutine for phase, split
      subroutine splitphasemag (na1,na2,na3,na4) ! Real, imaginary
      include "common.h"

      integer ia
      integer nbin; double precision dbin,d_a,d_M,d_P

      integer n_freq(0:50)
      integer d_freq(0:50)
      call duplicate (na1,na3,"magnitude",.false.)
      call duplicate (na1,na4,"phase",.false.)

      do ia=0,50; n_freq(ia)=0; d_freq(ia) = 0.0; enddo
      write(*,*) "in split-phase-subroutine",n(4,na1),na1,na2,na3,na4
      do ia=1,n(4,na1)
       
       d_M=sqrt (dcube(na1,ia)**2+dcube(na2,ia)**2) ! mag = sqrt [R^2+I^2}

       if (d_M.eq.0.00000000000) then           ! if mag = 0 set phase to 1000.
        d_P=1000.                               ! this acts as flag when differentiating.
       else
        d_P=atan2(dcube(na2,ia),dcube(na1,ia))       ! phase=acos (R/mag)
       endif
       if (d_P.GT.0) then 
        dcube(na3,ia)=d_M;dcube(na4,ia)=d_P
       else
        dcube(na3,ia)=0.;dcube(na4,ia)=0.
       endif
      enddo  
      write(*,*) "Done data-pass 1"
      call writefile("mag_pos.cube",na3,.false.)
      write(*,*) "Done data-write 0"
      call writefile("phs_pos.cube",na4,.false.)
      write(*,*) "Done data-write 1"
      do ia=1,n(4,na1)
       d_M=sqrt (dcube(na1,ia)**2+dcube(na2,ia)**2) ! mag = sqrt [R^2+I^2}
       if (d_M.eq.0.00000000000) then           ! if mag = 0 set phase to 1000.
        d_P=1000.                               ! this acts as flag when differentiating.
       else
        d_P=atan2(dcube(na2,ia),dcube(na1,ia))       ! phase=acos (R/mag)
       endif
       if (d_P.LT.0) then 
        dcube(na3,ia)=d_M;dcube(na4,ia)=d_P
       else
        dcube(na3,ia)=0.;dcube(na4,ia)=0.
       endif
      enddo  
      call writefile("mag_neg.cube",na3,.false.)
      call writefile("phs_neg.cube",na4,.false.)




      return
      end

! This subroutine calculates the inertia tensor just like "inertial" above, but for a 
! spinor. It requires 4 datasets: real/imag {alpha}, real/imag {beta}.
      subroutine inert_spinor(na1,na2,na3,na4)
      include "common.h"
      integer na1,na2,na3,na4,ia
      double precision d_theta
      double precision tens(4),pi_2,dev(2,4),dvec(2,2) 
      character answ*1
      logical a_answ

      a_answ=.true.
      pi_2=acos(0.)
      tens=0.
      do ia=1,n(4,na1)
       tens(1)=tens(1)+dcube(na1,ia)**2
       tens(1)=tens(1)+dcube(na3,ia)**2
       tens(2)=tens(2)+dcube(na2,ia)**2
       tens(2)=tens(2)+dcube(na4,ia)**2
       tens(3)=tens(3)+dcube(na1,ia)*dcube(na2,ia)
     &                +dcube(na3,ia)*dcube(na4,ia)
      enddo

      if (tens(3).eq.0.00) then
       d_theta=0.
      elseif ((tens(1)-tens(2)).eq.0.) then
       d_theta=0.5*pi_2
c       d_theta=0.25*3.1415926536
      else 
       d_theta=0.5*atan( (2*tens(3))/(tens(1)-tens(2) ) )
      endif 
      write(*,*) "Complex pseudo-inertial tensor: "
      write(*,*) tens(1),tens(3)
      write(*,*) tens(3),tens(2)
c      tens(4)=abs(tens(3)/(tens(2)+tens(1)))
 
      if (abs(tens(3)/(tens(2)+tens(1))).lt.0.0001) then
       write(*,*) "This matrix is diagonal (except numerical noise)."
       write (*,*) "Metric:",abs(tens(3)/(tens(2)+tens(1)))
       a_answ=.false.
       if (tens(1).LT.tens(2)) then
        write(*,*)"The datasets are the wrong way round. Take ",na2,na4,
     & "as real and ",na1,na3,"as imaginary."
       endif
      endif


!      write(*,*) d_theta, cos(d_theta),sin(d_theta)
      dvec(1,1)=cos(d_theta);dvec(1,2)=sin(d_theta)
      dvec(2,1)=cos(d_theta+pi_2);dvec(2,2)=sin(d_theta+pi_2)

      write(*,*) "Eigenvectors: "
      write(*,*) (dvec(1,ia),ia=1,2)
      write(*,*) (dvec(2,ia),ia=1,2)

      dev(1,1) = (tens(1)*dvec(1,1)+tens(3)*dvec(1,2))/dvec(1,1)
      dev(1,2) = (tens(3)*dvec(1,1)+tens(2)*dvec(1,2))/dvec(1,2)
      dev(2,1) = (tens(1)*dvec(2,1)+tens(3)*dvec(2,2))/dvec(2,1)
      dev(2,2) = (tens(3)*dvec(2,1)+tens(2)*dvec(2,2))/dvec(2,2)

      dev(1,3)=(dev(1,1)+dev(1,2))/2.
      dev(1,4)=abs(dev(1,1)-dev(1,3))
      dev(2,3)=(dev(2,1)+dev(2,2))/2.
      dev(2,4)=abs(dev(2,1)-dev(2,3))
      write(*,*) "Angle 1 :",d_theta
      write(*,*) "Angle 2 :",d_theta+pi_2
      write(*,*) "Vector 1: Eigenvalue ",dev(1,3),"+/-",dev(1,4)
      write(*,*) "Vector 2: Eigenvalue ",dev(2,3),"+/-",dev(2,4)

      if (dev(2,3).gt.dev(1,3)) d_theta=d_theta+pi_2

      do while (a_answ)
       write(*,*) "Would you like to rotate in the complex plane?"
       write(*,*) "[y/n]"
       read(*,*) answ
       if ( (answ.eq."Y").or.(answ.eq."y") ) then
        call rotation (na1,na2,-d_theta)
        call rotation (na3,na4,-d_theta)
        a_answ=.false.
        write(*,*) "Rotated."
        tens=0.
        do ia=1,n(4,na1)
         tens(1)=tens(1)+dcube(na1,ia)**2
         tens(1)=tens(1)+dcube(na3,ia)**2
         tens(2)=tens(2)+dcube(na2,ia)**2
         tens(2)=tens(2)+dcube(na4,ia)**2
         tens(3)=tens(3)+dcube(na1,ia)*dcube(na2,ia)
     &                  +dcube(na3,ia)*dcube(na4,ia)
        enddo
        write(*,*) "Complex pseudo-inertial tensor is now: "
        write(*,*) tens(1),tens(3)
        write(*,*) tens(3),tens(2)
       elseif ( (answ.eq."N").or.(answ.eq."n") ) then
        a_answ=.false.
       else
        write(*,*) "Answer not comprehended"
       endif
      enddo
      return
      end
   

