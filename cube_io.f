! subroutines for read/write/close; also, duplicate, status

      subroutine animation (na1,na2,na3)
      include "common.h"
      integer na1,na2,na3,ia,ib,ic
      double precision d_step, thc,ths   
      character stitle*40,c_nf*40
      
      d_step=4.*acos(0.)/(dble(na3))
      write(*,*) na3," steps, step size ",d_step
      do ia=0,(na3-1) ! loop over all na3 snapshots
       thc=cos(d_step*ia);ths=sin(d_step*ia)
       write(stitle,*) "Snapshot: ",ia ! this goes in the cubefile as the title
       ic=ia*360/na3
       if (ic .lt. 10) then
        write(c_nf,'(i1.1,A5)') ic,".cube"
       elseif (ic .lt. 100) then
        write(c_nf,'(i2.2,A5)') ic,".cube"
       else
        write(c_nf,'(i3.3,A5)') ic,".cube"
       endif
!       write(*,*) ia,c_nf,thc,ths
       call duplicate (na1,5,stitle,.FALSE.) ! create new ancillary data including new title
       do ib=1,n(4,na1)
        dcube(5,ib)=thc*dcube(na1,ib)+ths*dcube(na2,ib)
       enddo
       call writefile(c_nf, 5, .FALSE.) 

      enddo
      write(*,*) "Finished writing out ",na3," snapshots."
      if ( na3 .gt. 60) then
       write(*,*) "(That's a lot of snapshots.",
     &  "You probably don't need that many.)"
      endif
      return
      end      

      subroutine duplicate (l,k,c_inp,a_COPY) 
! a_COPY: if yes, then copy all the grid data.
! if no, then copy all the initial data (grid size, atoms) and leave
! the data on the grid uninitialised or whatever it was before.
      include "common.h"

      logical a_COPY
      integer k,l ! l = old file, k = new file
      character c_inp*40

      integer i,j
      do i=1,4
       n(i,k)=n(i,l)
       do j=1,3
        dr(i,j,k)=dr(i,j,l)
       enddo
      enddo
      nat(k)=nat(l);title(k)=c_inp
      do i=1,nat(k);nsym(i,k)=nsym(i,l)
       do j=1,3;xyz(i,j,k)=xyz(i,j,l)
      enddo;enddo

      if (a_COPY) then
       do i=1,n(4,k) 
        dcube(k,i)=dcube(l,i)
       enddo
      endif

      a_file(k)=.true.
      a_file(0)=.true.
      return; end

      subroutine readfile(c_inp,nfile,a_OUTPUT) !filename, dataset number; a_OUTPUT=1: write grid/atom data to terminal.

      include "common.h"

      character c_inp*40
      logical a_OUTPUT
      integer npl

      integer nr,k,i,n1,n2,n3
      double precision djunk
      nr=nfile+10

      open(unit=nr,file=c_inp,status="old")

      read(nr,*) title(nfile)
      read(nr,*) nat(nfile),(dr(4,k,nfile),k=1,3)
      if (a_OUTPUT) then 
       write(*,*) "Opened file ",c_inp, nr,nfile
       write(*,*) "Title: ",title(nfile)
       write(*,*) nat(nfile), " atoms" 
       write(*,*) "Origin: ",(dr(4,k,nfile),k=1,3)
       write(*,*) "Grid vectors: "
      endif
      do i=1,3
       read(nr,*) n(i,nfile),(dr(i,k,nfile),k=1,3)
       if (a_OUTPUT) write(*,*) n(i,nfile),(dr(i,k,nfile),k=1,3)
      enddo
      if (a_OUTPUT) write(*,*) "Atoms/centres: "
      do i=1,nat(nfile)
       read(nr,*) nsym(i,nfile),djunk,(xyz(i,k,nfile),k=1,3)
       if (a_OUTPUT) write(*,*) nsym(i,nfile),(xyz(i,k,nfile),k=1,3)
      enddo

      n(4,nfile)=n(1,nfile)*n(2,nfile)*n(3,nfile)

       if (a_OUTPUT) then
      write(*,*) "Cube file number ",nfile," contains ",
     & nat(nfile)," centres and ",n(4,nfile)," points. "
       endif
      if (n(4,nfile).GT.4500000) then
       write(*,*) "Too many points (max. 4500000)."
       stop
      endif

      read(nr,"(6F15.6)") (dcube(nfile,k),k=1,n(4,nfile)) 

      close(nr)
      a_file(nfile)=.true.
      a_file(0)=.true.
      return
      end

      subroutine writefile(c_inp2,nfile,a_SAFE) !Filename, dataset number; a_SAFE=0: overwrite old file

      include "common.h"
      character c_inp2*40
      integer nr,k,i,n1,n2,n3,ierr
      logical a_SAFE
      nr=nfile+10
803     format(2x,i3,3f12.6)
804     format(2x,i3,4f12.6)
      if (a_SAFE) then
       open(unit=nr,file=c_inp2,status="new",iostat=ierr)
       if (ierr.NE.0) then
        write(*,*) "File exists (or other problem). Not saving."
        return
       endif 
      else
       open(unit=nr,file=c_inp2,status="replace")
      endif

      write(nr,*) title(nfile)
      write(nr,*)
      write(nr,803) nat(nfile),(dr(4,k,nfile),k=1,3)
      do i=1,3
       write(nr,803) n(i,nfile),(dr(i,k,nfile),k=1,3)
      enddo
      do i=1,nat(nfile)
       write(nr,804) nsym(i,nfile),dble(nsym(i,nfile)),
     &    (xyz(i,k,nfile),k=1,3)
!       write(*,*) nsym(i,nfile),(xyz(i,k,nfile),k=1,3)
      enddo

      n(4,nfile)=n(1,nfile)*n(2,nfile)*n(3,nfile)



      write(nr,"(6F15.6)") (dcube(nfile,k),k=1,n(4,nfile)) 
      write(*,*) "Written file ",c_inp2
      close(nr)

      return
      end

      subroutine closefile(n_file)
      include "common.h"
      integer n_file,ia
      logical a_chk
      a_file(n_file)=.false.
      a_chk=.false.
      do ia=1,5;a_chk=a_chk.or.a_file(ia);enddo
      return
      end 

      subroutine cubestat()
      include "common.h"
      integer n_file

      do n_file=1,4
       if (a_file(n_file)) then
        write(*,*) n_file,": ",title(n_file)," : ",n(4,n_file)," points"

       else
        write(*,*) n_file,": Not opened "
       endif
      enddo

      return
      end 
