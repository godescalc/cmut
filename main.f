      program MAIN_CUBE
! manipulation of cube files
      include "common.h"
      character c_inp*40,c_i1*1,c_XY*1
      logical a_cont,a_XY
      integer i,j,k,n1,n2,n3,n4
      double precision d_inp

      a_file=.false.
      a_cont=.true.
      do while (a_cont)

      call cubestat()
      if (a_file(0).eqv..false.) then
       write(*,*) "No files opened. Please enter a filename to enter."
       write(*,*) "(Type 'select' for list of available files)"
       read(*,*) c_inp
       if (c_inp.eq."select") call filelist(c_inp)
       write(*,*) "Which file slot? (1-4)"
       read(*,*) n1
       call readfile(c_inp,n1,1)
      else
       write(*,*) "Options:"
       write(*,*) "[Q]uit, c[H]ange dataset title"
       write(*,*) "[R]ead, [W]rite, sa[F]e-write, [D]uplicate"
       write(*,*) "Sca[L]e, [A]dd, [S]ubtract, [M]ultiply, di[V]ide,
     & [E]xponentiate"
       write(*,*) "[P]hase/magnitude, spl[I]t phase/magnitue,"
       write(*,*) "[C]omplex direction, r[O]tate, a[N]imate" 
       write(*,*) "Comple[X] direction (spinor), reorien[T] spin"
       write(*,*) "For help, please see accompanying README file"
       read(*,*) c_i1
       select case (c_i1)
        case ("Q","q"); a_cont=.false.
        case ("Y","y"); a_cont=.true.
        case ("R","r")
          write(*,*)"Please enter filename "
          write(*,*) "(Type 'select' for list of available files)"
          read(*,*) c_inp
          if (c_inp.eq."select") call filelist(c_inp)
          write(*,*) "Enter file slot (1-4):";read(*,*) n1
          call readfile(c_inp,n1,1)
        case ("W","w"); write(*,*) "Enter file slot (1-4):";read(*,*) n1
          write(*,*) "Enter filename to write to.";read(*,*) c_inp
          call writefile(c_inp,n1,0)
        case ("F","f"); write(*,*) "Enter file slot (1-4):";read(*,*) n1
          write(*,*) "Enter filename to write to.";read(*,*) c_inp
          call writefile(c_inp,n1,1)
        case ("D","d")
          write(*,*)"Enter file slots (source, destination)"
          read(*,*) n1,n2
          if (a_file(n1)) then
            write(*,*) "Enter new title:"
            read(*,*) c_inp; call duplicate(n1,n2,c_inp,1)
          else; write (*,*) "File slot ",n1," is empty!";endif
        case ("H","h"); write(*,*) "Which file slot?";read(*,*) n1
          write(*,*) "New title: ";read(*,*) c_inp; title(n1)=c_inp
        case("L","l")
          write(*,*) "Enter file slot, scaling factor:"
          read(*,*) n1,d_inp; call scalecb(n1,d_inp)
        case("A","a")
          write(*,*) "Enter files to be added, file for result,",
     &    "new title for result "
          read(*,*) n1,n2,n3,c_inp; call mathops(n1,n2,n3,c_inp,1)
        case("S","s")
          write(*,*) "Enter files to be subtracted, file for result,",
     &    "new title for result "
          read(*,*) n1,n2,n3,c_inp; call mathops(n1,n2,n3,c_inp,2)
        case("M","m")
          write(*,*) "Enter files to be multiplied, file for result,",
     &    "new title for result "
          read(*,*) n1,n2,n3,c_inp; call mathops(n1,n2,n3,c_inp,3)
        case("V","v")
          write(*,*) "Enter files to be divided, file for result,",
     &    "new title for result "
          read(*,*) n1,n2,n3,c_inp; call mathops(n1,n2,n3,c_inp,4)
        case("P","p")
          write(*,*) "Enter files for real, imaginary,",
     &       " magnitude, phase:"
          read (*,*) n1,n2,n3,n4; call phasemag(n1,n2,n3,n4)
        case("I","i")
          write(*,*) "Enter files for real, imaginary, outfiles"
          read (*,*) n1,n2,n3,n4; call splitphasemag(n1,n2,n3,n4)
        case("C","c")
          write(*,*) "Enter files for real, imaginary:"
          read(*,*) n1,n2; call inertial (n1,n2,d_inp)
c          write(*,*) "Complex number: ",cos(d_inp)," + i",sin(d_inp)
        case("X","x")
          write(*,*) "Enter files for re, im {A}, re, im {B}"
          read(*,*) n1,n2,n3,n4; call inert_spinor (n1,n2,n3,n4)
        case("T","t")
          write(*,*) "Enter files for re, im (alpha), re, im (beta)"
          read (*,*) n1,n2,n3,n4
          a_XY=.true.
          do while (a_XY)
           write(*,*) "Reorient to X or Y?"   
           read(*,*) c_XY
           select case(c_XY)
            case("X","x")
             call reorient(n1,n2,n3,n4,.true.);a_XY=.false.
            case("Y","y")
             call reorient(n1,n2,n3,n4,.false.);a_XY=.false.
            case default; write(*,*) "Please enter X or Y"
           end select
          enddo    
        case("O","o")
          write(*,*) "Enter files for real, imaginary:"
          read(*,*) n1,n2; write(*,*) "Angle to rotate by: "
          read(*,*) d_inp; call rotation (n1,n2,d_inp)
        case("N","n")
          write(*,*) "Enter files for real, imaginary:"
          read(*,*) n1,n2
          write(*,*) "Number of snapshots (will be rounded up to ",
     & "a factor of 360)" 
          read(*,*) n3; call checksn(n3); call animation(n1,n2,n3)
        case default; write(*,*) "Not recognised or not implemented"
       end select
      endif
      enddo

      stop
      end
 
      subroutine filelist(c_ff)
      real :: r
      integer :: igf,reason,NstationFiles,iFile
      character(LEN=100) :: stationFileNames(50)
      character c_ff*20
      ! get the files
      call system('ls *.cube > fileContents.txt')
      open(31,FILE='fileContents.txt',action="read")
      !how many
      igf = 0
      do
       read(31,FMT='(a)',iostat=reason) r
!       write(*,*) r
       if (reason/=0) EXIT
       igf = igf+1
      end do

      NstationFiles = igf
      write(*,'(a,I0)') "Number of files: " , NstationFiles
      rewind(31)
      do igf = 1,NstationFiles
       read(31,'(a)') stationFileNames(igf)

       write(*,'(I3,a,a)') igf,": ", trim(stationFileNames(igf)) 
      end do
100   continue
      write(*,*) "Please select a file by number."
      read(*,*) iFile
      if ( (iFile.GT.igf).or.(iFile.LE.0) ) goto 100
      c_ff=trim(stationFileNames(iFile))
      write(*,*) "You have selected ",c_ff
      close(31)
       return;end
